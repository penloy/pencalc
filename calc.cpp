#include <iostream>
#include <string.h>
#include <vector>

float add(std::vector<float> v)
{
    float result = 0;
    for (const auto x : v)
    {
        result+=x;
    }
    return result;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Not enough args. Please provide more arguments." << std::endl;
        return 0;
    }

    std::vector<float> v;

    if (strcmp(argv[1], "add") == 0)
    {
        if (argc < 4)
        {
            std::cout << "Not enough arguments for addition" << std::endl;
            return 0;
        }
        std::cout << "Addition time" << std::endl;
        for (int i = 2; i < argc; i++)
        {
            v.push_back(std::stod(argv[i], 0));
        }
        std::cout << add(v);
    }
}
